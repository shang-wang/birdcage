This is a home automation simulation project that uses a hub to control some thermostats using RESTful api.

# FILES

    - hub.py            program that simulates a hub
    - thremo.py         program that simulates a thermo
    - requirements.txt  python dependencies
    - README.md         this file

# Usage

## install

To start, simply create a python virtual environment and install dependencies:

    pip install -r requirements.txt

## How to run the program

The hub program needs to run first in order for thermos to be registered. It's running on a fixed port localhost:3000, so it's as simple as:

    python hub.py

The thermo program takes 2 parameters: `-p` represents the port that the thermo is running on, `-t` represents the tid(thermo id) as a unique identifier for current thermo. So to run a thermo, do:

    python thermo.py -p 8001 -t 1

Multiple thermos could be run at the same time, as long as they are running on different ports and have unique tids.

# API usage

## thermo.py

The thermo app has following functionality:

### 1. Report thermo status

This endpoint accepts `GET` request and returns the status of the thermo. 

Sample request:

    http://localhost:8001/api/v1/status/1/

Sample response:

    {"status": "online"}


### 2. Thermo nickname
This endpoint accepts `POST` request with parameter `nickname` and setting current thermo's nick name. It also accepts `GET` request and return current nickname of the thermo.

Sample `POST` request:

    import requests
    r = requests.post('http://localhost:8001/api/v1/nickname/', data={'nickname': 'John'})

Sample response:

    {"status": "success", "message": "nickname is set to john"}

Sample `GET` request:

    http://localhost:8001/api/v1/nickname/

Sample reponse:

    {"status": "success", "nickname": "john"}


### 3. Target temperature
This endpoint accepts `POST` request with parameter `target_temperature` and set a target temperature to the thermo. It also accepts `GET` request and return current target temperature.

Sample `POST` request:

    import requests
    r = requests.post('http://localhost:8001:/api/v1/target-temp/', data={'target_temperature': 70.8})


Sample response:

    {"status": "success", "message": "Target temperature has changed to 78.1"}

Sample `GET` request:

    http://localhost:8001/api/v1/target-temp/

Sample response:
    
    { "status": "success", "target_temperature": 78.1}


### 4. Room temperature
This endpoint accepts `GET` request and returns current room temperature.

Sample `GET` request:

    http://localhost:8001/api/v1/room-temp/

Sample response:

    {"status": "success", "room_temperature": 73.51}


### 5. Target temperature history
This endpoint accepts `GET` request and returns a list of target temperature as history.

Sample `GET` request:

    http://localhost:8001/api/v1/target-temp-history/

Sample response:

    {"status": "success", "target_temperature_history": [70.5, 78.1, 65]}

### 6. Current thermo's information
This endpoint accepts `GET` request and returns current thermos basic information.

Sample `GET` request:

    http://localhost:8001/api/v1/thermo-info/

Sample response:

    {"status": "success", "tid": "1", "target_temperature": 70, "nickname": "John", "room_temperature": 68}

## hub.py

The hub app has following functionality:

### 1. List all registered thermos
This endpoint accepts `GET` request and returns a list of registered thermos and their online status.

Sample `GET` request:

    http://localhost:3000/api/v1/list-thermos/

Sample response:

    {"status": "success", "thermos": [{"tid": "1", "port": 8001, "status": "online"}, {"tid": "2", "port": 8002, "status": "offline"}]}

### 2. Set target temperature of a thermo
This endpoint accepts `POST` request with parameter `tid` and `temperature`, then set the thermo that has the tid in the request with the target temperature.

Sample `POST` request:

    import requests
    r = requests.post('http://localhost:3000/api/v1/set-target-temp/', data={'tid': '1', 'temperature': 70})

Sample response:

    {"status": "success", "message": "The target temperature has been set to 70"}

### 3. Set target temperature for all thermos
This endpoint accepts `POST` request with parameter `temperature` and set all thermos to the target temperature.

Sample `POST` request:

    import requests
    r = requests.post('http://localhost:3000/api/v1/set-all-target-temp/', data={'temperature': 70})

Sample response:

    {"status": "success", "message": "All thermo target temperature have been set to 70"}

### 4. Get average room temperature

This endpoint accepts `GET` request and return average temperature among all the thermos.

Sample `GET` request:

    http://localhost:3000/api/v1/get-avg-temp/

Sample response:
    
    {"status": "success", "room_temperature": 73.1}

# How it works

The hub and thermo app are created using `tornado` framework and `requests` python url library. When hub app starts, it periodically pings all registered thermos by calling the status api on each thermo. 

When thermo starts, it forks a thread before entering into event loop. The thread is responsible for registering current thermo to the hub. It does so by sending a `POST` request to hub with current `tid` and `port` as post parameters. Hub in return does a handshake by pinging back current thermo status api. If status api returns "online" response, it adds current thermo to it's internal data structure.