#!/usr/bin/env python

import tornado.ioloop
import tornado.web
import requests


ONE_MINUTE = 1000 * 60


class Thermo(object):
    status = 'online'

    def __init__(self, tid, port):
        self.tid = tid
        self.port = port

    def __repr__(self):
        return 'Thermo tid %s on port %s' % (self.tid, self.port)


class ThermoRegisterHandler(tornado.web.RequestHandler):
    """
    Register a thermo handler. Get tid and port info for the thermo
    and do a handshake to make sure thermo is online
    """
    def post(self):
        tid = self.get_argument('tid', None)
        port = self.get_argument('port', None)
        if tid and port:
            new_thermo = Thermo(tid, port)
            r = requests.get('http://localhost:%s/api/v1/status/%s/' % (port, tid)).json()
            if r['status'] == 'online':
                self.application.thermos[tid] = new_thermo
                self.write({'status': 'success',
                           'message': 'Thermostats %s is registered successfully!' % tid})


class ListThermoHandler(tornado.web.RequestHandler):
    """
    List all registered thermo and their online status
    We won't actually check each thermo's status, just loop through
    because we have periodic task that pings each of them
    """
    def get(self):
        result = {'status': 'success',
                  'thermos': []}
        for tid, thermo in self.application.thermos.iteritems():
            result['thermos'].append({'tid': tid,
                                      'port': thermo.port,
                                      'status': thermo.status})
        self.write(result)


class SetTargetTempHandler(tornado.web.RequestHandler):
    """
    Set target temperature for a thermo
    """
    def post(self):
        tid = self.get_argument('tid', None)
        temp = self.get_argument('temperature', None)
        if tid and temp:
            try:
                port = self.application.thermos[tid].port
                requests.post('http://localhost:%s/api/v1/target-temp/' % port,
                              data={'target_temperature': float(temp)}).json()
                self.write({'status': 'success',
                            'message': 'The target temprature has been set to %s' % temp})
            except requests.exceptions.ConnectionError:
                self.write({'status': 'error',
                            'message': 'The thermo is not online'})
        else:
            self.write({'status': 'error',
                        'message': 'Please pass tid and temperature as post parameters'})


class SetAllTargetTempHandler(tornado.web.RequestHandler):
    def post(self):
        temp = self.get_argument('temperature', None)
        if temp:
            for thermo in self.application.thermos.itervalues():
                try:
                    requests.post('http://localhost:%s/api/v1/target-temp/' % thermo.port,
                                  data={'target_temperature': float(temp)}).json()
                except requests.exceptions.ConnectionError:
                    pass
            self.write({'status': 'success',
                        'message': 'All thermo target temperatures have been set to %s' % temp})
        else:
            self.write({'status': 'error',
                        'message': 'Please pass temperature as post parameter'})


class GetAvgTempHandler(tornado.web.RequestHandler):
    def get(self):
        total, counter = 0.0, 0
        for thermo in self.application.thermos.itervalues():
            try:
                r = requests.get('http://localhost:%s/api/v1/room-temp/' % thermo.port).json()
                if r['status'] == 'success':
                    total += r['room_temperature']
                    counter += 1
            except requests.exceptions.ConnectionError:
                pass
        avg = round(total / counter, 2) if counter else 0
        self.write({'status': 'success',
                    'average_temperature': avg})


class HubApplication(tornado.web.Application):
    def __init__(self, thermos):
        self.thermos = thermos
        handlers = [
            (r'/api/v1/register/', ThermoRegisterHandler),
            (r'/api/v1/list-thermos/', ListThermoHandler),
            (r'/api/v1/set-target-temp/', SetTargetTempHandler),
            (r'/api/v1/set-all-target-temp/', SetAllTargetTempHandler),
            (r'/api/v1/get-avg-temp/', GetAvgTempHandler),
        ]
        tornado.web.Application.__init__(self, handlers)


def update_thermo_status(thermos):
    for thermo in thermos.itervalues():
        try:
            status = requests.get('http://localhost:%s/api/v1/status/%s/' % (thermo.port,
                                                                             thermo.tid))
            if status == 'online':
                thermo.status = 'online'
            else:
                thermo.status = 'offline'
        except requests.exceptions.ConnectionError:
            thermo.status = 'offline'


def main():
    thermos = {}
    app = HubApplication(thermos)
    app.listen(3000)
    # refresh ping every minute
    tornado.ioloop.PeriodicCallback(lambda: update_thermo_status(thermos), ONE_MINUTE).start()
    tornado.ioloop.IOLoop.current().start()

if __name__ == '__main__':
    main()
