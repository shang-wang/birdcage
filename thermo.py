#!/usr/bin/env python

import tornado.ioloop
import tornado.web
import requests
import time
import threading
import random
import argparse

HUB_BASE_URL = 'http://localhost:3000'
REGISTER_ENDPOINT = '/api/v1/register/'


class ThermoStatusHandler(tornado.web.RequestHandler):
    """
    Thermo status api, used for hub to ping/handshake
    """
    def get(self, tid):
        if tid == self.application.tid:
            self.write({'status': 'online'})
        else:
            self.write({'status': 'error',
                       'message': 'Invalid tid'})


class NicknameHandler(tornado.web.RequestHandler):
    """
    Set or get thermo nickname
    """
    def get(self):
        if self.application.nickname:
            self.write({'status': 'success',
                        'nickname': self.application.nickname})
        else:
            self.write({'status': 'error',
                        'message': 'Please set a nickname first'})

    def post(self):
        nickname = self.get_argument('nickname', None)
        if nickname:
            self.application.nickname = nickname
            self.write({'status': 'success',
                        'message': 'nickname is set to %s' % nickname})
        else:
            self.write({'status': 'error',
                        'message': 'Please pass nickname as post parameter'})


class TargetTempHandler(tornado.web.RequestHandler):
    """
    Set or get thermo's target temperature
    """
    def get(self):
        if self.application.target_temp:
            self.write({'status': 'success',
                        'target_temperature': self.application.target_temp})
        else:
            self.write({'status': 'error',
                        'message': 'Please set a target temperature first'})

    def post(self):
        target_temp = self.get_argument('target_temperature', None)
        if target_temp:
            self.application.target_temp = float(target_temp)
            self.application.target_temp_history.append(float(target_temp))
            self.write({'status': 'success',
                        'message': 'Target temperature has changed to %s' % target_temp})
        else:
            self.write({'status': 'error',
                        'message': 'Please pass target_temperature as post parameter'})


class RoomTempHandler(tornado.web.RequestHandler):
    """
    Get current room temperature
    """
    def get(self):
        self.write({'status': 'success',
                    'room_temperature': round(random.uniform(65, 75), 2)})


class TargetTempHistoryHandler(tornado.web.RequestHandler):
    """
    Get history of target temperature
    """
    def get(self):
        self.write({'status': 'success',
                   'target_temperature_history': self.application.target_temp_history})


class ThermoInfoHandler(tornado.web.RequestHandler):
    """
    Return current thermo's information
    """
    def get(self):
        self.write({'status': 'success',
                    'tid': self.application.tid,
                    'target_temperature': self.application.target_temp,
                    'nickname': self.application.nickname,
                    'room_temperature': round(random.uniform(65, 75), 2)})


class ThermoApplication(tornado.web.Application):
    def __init__(self, tid):
        # global variable
        self.tid = tid
        self.target_temp = None
        self.target_temp_history = []
        self.nickname = None

        handlers = [
            (r'/api/v1/status/(\d+)/',        ThermoStatusHandler),
            (r'/api/v1/nickname/',            NicknameHandler),
            (r'/api/v1/target-temp/',         TargetTempHandler),
            (r'/api/v1/room-temp/',           RoomTempHandler),
            (r'/api/v1/target-temp-history/', TargetTempHistoryHandler),
            (r'/api/v1/thermo-info/',         ThermoInfoHandler),
        ]
        tornado.web.Application.__init__(self, handlers)


def register(tid, port):
    """
    A register function to register current thermo to the hub
    pass along the tid and the port to hub
    This is run as a separate thread in order not to block the ioloop

    hub will do handshake through ThermoStatusHandler
    """
    time.sleep(1)
    print 'registering...'
    url = HUB_BASE_URL + REGISTER_ENDPOINT
    r = requests.post(url, data={'tid': tid, 'port': port}).json()
    print r
    return


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p',
                        action='store',
                        dest='port',
                        help='Port for the thermo to run on')

    parser.add_argument('-t',
                        action='store',
                        dest='tid',
                        help='Unique tid for the thermo')

    args = parser.parse_args()
    if not vars(args)['port'] or not vars(args)['tid']:
        parser.print_help()
        parser.exit(1)

    port = args.port
    tid = args.tid

    app = ThermoApplication(tid)
    app.listen(port)
    main_loop = tornado.ioloop.IOLoop.instance()

    # use a separate thread to register so that we don't block the io loop
    register_thread = threading.Thread(target=register, args=(tid, port))
    register_thread.daemon = True
    register_thread.start()

    main_loop.start()


if __name__ == '__main__':
    main()
